/*
 * TODO: Remove the numerous alerts and make one to have the user complete
 * required inputs.
 *
 */


function _createTableHeader(table) {

    var row = document.createElement("tr");
    row.id = "header"

    th = document.createElement("th");
    th.appendChild(document.createTextNode("First Name"));
    row.appendChild(th);

    th = document.createElement("th");
    th.appendChild(document.createTextNode("Last Name"));
    row.appendChild(th);

    th = document.createElement("th");
    th.appendChild(document.createTextNode("University"));
    row.appendChild(th);

    th = document.createElement("th");
    th.appendChild(document.createTextNode("Research"));
    row.appendChild(th);

    th = document.createElement("th");
    th.appendChild(document.createTextNode("Email"));
    row.appendChild(th);

    table.appendChild(row);

}

/*
 * Create html table. This is the table we will be searching.
 */
function makeTable(data) {

    /* This is like creating the <table> tag */
    var table = document.createElement("table");
    table.id = "databaseTable"

    _createTableHeader(table);

    for(var i = 0; i < data.length; i++) {

        var row = document.createElement("tr");
        var td;

        td = document.createElement("td");
        td.appendChild(document.createTextNode(data[i].firstName));
        row.appendChild(td);

        td = document.createElement("td");
        td.appendChild(document.createTextNode(data[i].lastName));
        row.appendChild(td);

        td = document.createElement("td");
        td.appendChild(document.createTextNode(data[i].university));
        row.appendChild(td);

        td = document.createElement("td");
        td.appendChild(document.createTextNode(data[i].research));
        row.appendChild(td);

        td = document.createElement("td");
        td.appendChild(document.createTextNode(data[i].email));
        row.appendChild(td);

        table.appendChild(row);

    }

    /* This is like creating the </table> tag */
    document.body.appendChild(table);
}

/*
 * Split up string input by spaces.
 *
 * Return an array of inputs.
 *
 */
function splitString(string) {
    // Match spaces and remove them.
}

/*
 * Search function: thanks to:
 * https://stackoverflow.com/questions/9127498/how-to-perform-a-real-time-search-and-filter-on-a-html-table
 */
function search() {

    var input = document.getElementById("searchInput");
    var filter = input.value.toUpperCase();
    var table = document.getElementById("databaseTable");
    var tr = table.getElementsByTagName("tr");
    var found = false;

    for (var i=0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td");

        for (var j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                found = true;
            }
        }

        if (found) {
            tr[i].style.display = "";
            found = false;
        } else {
            if (tr[i].id != "header") {
                tr[i].style.display = "none";
            }
        }

    }
}
