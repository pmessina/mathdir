# Math Directory

Math Directory is a web page that stores short profiles of mathematicians,
contact info, areas of research, etc. The webpage will have a number of sorting
methods to find whomever is searched.

# Ultimate Goals 

* Keep the code clean and easy to edit for others to come in and work on it.
* Simplicity in use.
